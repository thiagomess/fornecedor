package br.com.thiago.microservices.fornecedor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.thiago.microservices.fornecedor.model.InfoFornecedor;
import br.com.thiago.microservices.fornecedor.repository.InfoRepository;

@Service
public class InfoService {
	
	@Autowired
	private InfoRepository repository;

	public InfoFornecedor getInfoPorEstado(String estado) {
		
		return repository.findByEstado(estado);
	}

}
