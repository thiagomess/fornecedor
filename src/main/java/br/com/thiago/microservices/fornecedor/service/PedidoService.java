package br.com.thiago.microservices.fornecedor.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.thiago.microservices.fornecedor.dto.ItemDoPedidoDTO;
import br.com.thiago.microservices.fornecedor.model.Pedido;
import br.com.thiago.microservices.fornecedor.model.PedidoItem;
import br.com.thiago.microservices.fornecedor.model.Produto;
import br.com.thiago.microservices.fornecedor.repository.PedidoRepository;
import br.com.thiago.microservices.fornecedor.repository.ProdutoRepository;

@Service
public class PedidoService {
	
	@Autowired
	private PedidoRepository pedidoRepository;
	
	@Autowired
	private ProdutoRepository produtoRepository;

	public Pedido realizaPedido(List<ItemDoPedidoDTO> itens) {
		
		if(itens == null) {
			return null;
		}
		
		List<PedidoItem> pedidoItens = toPedidoItem(itens);
		BigDecimal valorTotal = calculaValorTotal(pedidoItens);
				
		Pedido pedido = new Pedido(pedidoItens);
		pedido.setValorTotal(valorTotal);
		pedido.setTempoDePreparo(itens.size());
		return pedidoRepository.save(pedido);
	}
	
	public Pedido getPedidoPorId(Long id) {
		return this.pedidoRepository.findById(id).orElse(new Pedido());
	}

	private List<PedidoItem> toPedidoItem(List<ItemDoPedidoDTO> itens) {
		
		List<Long> idsProdutos = itens
				.stream()
				.map(item -> item.getId())
				.collect(Collectors.toList());
		
		List<Produto> produtosDoPedido = produtoRepository.findByIdIn(idsProdutos);
		
		//varre a lista de produtos para setar cda item em seu objeto PedidoItem.
		List<PedidoItem> pedidoItens = itens
			.stream()
			.map(item -> {
				Produto produto = produtosDoPedido
						.stream()
						.filter(p -> p.getId() == item.getId())
						.findFirst().get();
				
				PedidoItem pedidoItem = new PedidoItem();
				pedidoItem.setProduto(produto);
				pedidoItem.setQuantidade(item.getQuantidade());
				return pedidoItem;
			})
			.collect(Collectors.toList());
		return pedidoItens;
	}

	private BigDecimal calculaValorTotal(List<PedidoItem> pedidoItens) {
		BigDecimal valorTotal = pedidoItens.stream()
								.map(p -> p.getProduto().getPreco().multiply(new BigDecimal(p.getQuantidade())))
								.reduce(BigDecimal.ZERO, BigDecimal::add);
		return valorTotal;
	}
}
