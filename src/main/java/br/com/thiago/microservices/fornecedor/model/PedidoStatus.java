package br.com.thiago.microservices.fornecedor.model;

public enum PedidoStatus {
	RECEBIDO, PRONTO, ENVIADO;
}
