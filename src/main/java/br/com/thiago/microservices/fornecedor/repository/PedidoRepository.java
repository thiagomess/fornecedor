package br.com.thiago.microservices.fornecedor.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.thiago.microservices.fornecedor.model.Pedido;

public interface PedidoRepository extends CrudRepository<Pedido, Long>{

}
